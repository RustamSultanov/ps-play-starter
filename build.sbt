name := "ps-play-starter"
organization := "com.ps-play-starter"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.3"

resolvers += Resolver.jcenterRepo

resolvers += "Sonatype snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

val playSilhouetteVersion = "7.0.0"
val slickPgVersion = "0.19.2"

libraryDependencies ++= Seq(
  "org.flywaydb" % "flyway-core" % "6.5.4",
  "com.softwaremill.macwire" %% "macros" % "2.3.7" % "provided",
  "org.postgresql" % "postgresql" % "42.2.16",
  "com.github.tminglei" %% "slick-pg" % slickPgVersion,
  "com.github.tminglei" %% "slick-pg_circe-json" % slickPgVersion,
  "be.venneborg" %% "slick-refined" % "0.5.0",
  "com.typesafe.play" %% "play-slick" % "5.0.0",
  "com.github.tototoshi" %% "slick-joda-mapper" % "2.4.2",
  "com.mohiva" %% "play-silhouette-password-bcrypt" % playSilhouetteVersion,
  "com.mohiva" %% "play-silhouette-persistence" % playSilhouetteVersion,
  "com.mohiva" %% "play-silhouette-crypto-jca" % playSilhouetteVersion,
  "com.mohiva" %% "play-silhouette-testkit" % playSilhouetteVersion % Test,
  "org.webjars" %% "webjars-play" % "2.8.0-1",
  "org.webjars.npm" % "popper.js" % "1.16.1",
  // we only support Boostrap 3 at the moment
  "org.webjars.npm" % "bootstrap" % "3.4.1", // scala-steward:off
  "org.webjars.npm" % "jquery" % "3.5.1",
  "org.webjars.npm" % "zxcvbn" % "4.4.2",
  "org.webjars.npm" % "rollbar" % "2.15.2",
  "com.iheart" %% "ficus" % "1.5.0",
  "com.typesafe.play" %% "play-mailer" % "8.0.1",
  "com.enragedginger" %% "akka-quartz-scheduler" % "1.8.4-akka-2.6.x",
  // play-bootstrap: we use ***-B3 version for Bootstrap 3 compatibility
  "com.adrianhurt" %% "play-bootstrap" % "1.6.1-P28-B3",
  "org.apache.commons" % "commons-lang3" % "3.11",
  "org.typelevel" %% "cats-core" % "2.1.1",
  "org.typelevel" %% "mouse" % "0.25",
  "io.circe" %% "circe-core" % "0.13.0",
  "io.circe" %% "circe-generic" % "0.13.0",
  "io.circe" %% "circe-generic-extras" % "0.13.0",
  "io.circe" %% "circe-parser" % "0.13.0",
  "io.circe" %% "circe-literal" % "0.13.0",
  "io.circe" %% "circe-refined" % "0.13.0",
  "com.beachape" %% "enumeratum" % "1.6.1",
  "com.beachape" %% "enumeratum-circe" % "1.5.22",
  "com.beachape" %% "enumeratum-slick" % "1.6.0",
  "com.github.pureconfig" %% "pureconfig" % "0.12.2",
  "com.github.pureconfig" %% "pureconfig-enumeratum" % "0.12.2",
  "eu.timepit" %% "refined"                 % "0.9.15",
  "eu.timepit" %% "refined-pureconfig"      % "0.9.15",
  "eu.timepit" %% "refined-cats"            % "0.9.15",
  "com.rollbar" % "rollbar" % "0.5.4",
  "com.papertrailapp" % "logback-syslog4j" % "1.0.0",
  specs2 % Test,
  ehcache,
  filters
)

scalacOptions ++= Seq(
  "-deprecation", // Emit warning and location for usages of deprecated APIs.
  "-feature", // Emit warning and location for usages of features that should be imported explicitly.
  "-unchecked", // Enable additional warnings where generated code depends on assumptions.
//  "-Xfatal-warnings", // Fail the compilation if there are any warnings.
  "-Ywarn-dead-code", // Warn when dead code is identified.
//  "-Ywarn-numeric-widen", // Warn when numerics are widened.
  // Play has a lot of issues with unused imports and unsued params
  // https://github.com/playframework/playframework/issues/6690
  // https://github.com/playframework/twirl/issues/105
  "-Xlint:-unused,-byname-implicit,_",
)

scalafmtOnCompile := true

herokuAppName in Compile := "ps-play-starter-stage"

herokuBuildpacks in Compile := Seq(
  "heroku/metrics",
  "heroku/jvm",
)

sources in (Compile, doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false

PlayKeys.devSettings ++= Seq(
  "play.server.http.idleTimeout" -> "60s",
  "play.server.akka.requestTimeout" -> "40s",
)

pipelineStages := Seq(digest, gzip)

val herokuApiKey =
  taskKey[String]("Heroku api key")
val stagePlayHttpSecret =
  taskKey[String]("staging play http secret")
val stagePlayMailerUser =
  taskKey[String]("staging play mailer user")
val stagePlayMailerPassword =
  taskKey[String]("staging play mailer password")
val stageSilhouetteAuthenticatorSignerKey =
  taskKey[String]("staging silhouette authenticator signer key")
val stageSilhouetteAuthenticatorCrypterKey =
  taskKey[String]("staging silhouette authenticator crypter key")
val logDrainURL =
  taskKey[String]("log drain URL")
val herokuAppExists =
  taskKey[Boolean]("check that Heroku app exists")
val herokuAppCreate =
  taskKey[Boolean]("create Heroku app")
val herokuAppDelete =
  taskKey[Boolean]("delete Heroku app")
val herokuLogDrainCreate =
  taskKey[Boolean]("create Heroku log drain")
val herokuDbAttachmentExists =
  taskKey[Boolean]("check that db is attached to Heroku app")
val herokuDbAttachmentCreate =
  taskKey[Boolean]("attach db to Heroku app")
val herokuConfigVarsExist =
  taskKey[Boolean]("check that db is attached to Heroku app")
val herokuConfigVarsCreate =
  taskKey[Boolean]("init Heroku app's vars")
val herokuFormationBatchUpdate =
  taskKey[Boolean]("scale Heroku app")

val dbId = "ps-play-starter-stage-db"

herokuApiKey := {
  System.getenv("HEROKU_API_KEY")
}

stagePlayHttpSecret := {
  System.getenv("STAGE_PLAY_HTTP_SECRET")
}
stagePlayMailerUser := {
  System.getenv("STAGE_PLAY_MAILER_USER")
}
stagePlayMailerPassword := {
  System.getenv("STAGE_PLAY_MAILER_PASSWORD")
}
stageSilhouetteAuthenticatorSignerKey := {
  System.getenv("STAGE_SILHOUETTE_AUTHENTICATOR_SIGNER_KEY")
}
stageSilhouetteAuthenticatorCrypterKey := {
  System.getenv("STAGE_SILHOUETTE_AUTHENTICATOR_CRYPTER_KEY")
}
logDrainURL := {
  System.getenv("LOG_DRAIN_URL")
}

herokuAppExists := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val appName = (Compile / herokuAppName).value
  val svc = url(
    s"https://api.heroku.com/apps/$appName",
  )
    .addHeader("Accept", "application/vnd.heroku+json; version=3")
    .addHeader("Authorization", s"Bearer ${herokuApiKey.value}")
    .GET
  val result =
    Await.result(Http.default(svc) map (_.getStatusCode == 200), 30.seconds)
  println(s"$appName Heroku app exists: $result")
  result
}

herokuAppCreate := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val apiKey = herokuApiKey.value
  val appName = (Compile / herokuAppName).value
  val result =
    if (!herokuAppExists.value) {
      val svc = url("https://api.heroku.com/apps")
        .addHeader("Accept", "application/vnd.heroku+json; version=3")
        .addHeader("Authorization", s"Bearer $apiKey")
        .addHeader("Content-Type", "application/json")
        .setStringBody(s"""{"name": "$appName"}""")
        .POST
      Await.result(Http.default(svc) map ( _.getStatusCode == 201 ), 30.seconds)
    } else false
  println(s"$appName Heroku app created: $result")
  result
}

herokuAppDelete := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val apiKey = herokuApiKey.value
  val appName = (Compile / herokuAppName).value
  val result =
    if (herokuAppExists.value) {
      val svc = url(
        s"https://api.heroku.com/apps/$appName",
      )
        .addHeader("Accept", "application/vnd.heroku+json; version=3")
        .addHeader("Authorization", s"Bearer $apiKey")
        .DELETE
      Await.result(Http.default(svc) map ( _.getStatusCode == 200 ), 30.seconds)
    } else false
  println(s"$appName Heroku app deleted: $result")
  result
}

herokuLogDrainCreate := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val apiKey = herokuApiKey.value
  val appName = (Compile / herokuAppName).value
  val logDrainURLValue = logDrainURL.value
  val result =
    if (herokuAppExists.value) {
      val svc = url(s"https://api.heroku.com/apps/$appName/log-drains")
        .addHeader("Accept", "application/vnd.heroku+json; version=3")
        .addHeader("Authorization", s"Bearer $apiKey")
        .addHeader("Content-Type", "application/json")
        .setStringBody(
          s"""{"url": "$logDrainURLValue"""
        )
        .POST
      Await.result(Http.default(svc) map ( _.getStatusCode == 201 ), 30.seconds)
    } else false
  println(s"$appName Heroku log drain created: $result")
  result
}

herokuDbAttachmentExists := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val appName = (Compile / herokuAppName).value
  val svc = url(s"https://api.heroku.com/apps/$appName/addon-attachments/$dbId")
    .addHeader("Accept", "application/vnd.heroku+json; version=3")
    .addHeader("Authorization", s"Bearer ${herokuApiKey.value}")
    .GET
  val result =
    Await.result(Http.default(svc) map (_.getStatusCode == 200), 30.seconds)
  println(s"$appName Heroku app DB attachment exists: $result")
  result
}

herokuDbAttachmentCreate := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val apiKey = herokuApiKey.value
  val appName = (Compile / herokuAppName).value
  val result =
    if (herokuAppExists.value) {
      val svc = url("https://api.heroku.com/addon-attachments")
        .addHeader("Accept", "application/vnd.heroku+json; version=3")
        .addHeader("Authorization", s"Bearer $apiKey")
        .addHeader("Content-Type", "application/json")
        .setStringBody(
          s"""{
             |  "app": "$appName",
             |  "addon": "$dbId",
             |  "name": "DATABASE"
             |}""".stripMargin)
        .POST
      Await.result(Http.default(svc) map ( _.getStatusCode == 201 ), 30.seconds)
    } else false
  println(s"$appName Heroku app DB attachment created: $result")
  result
}

herokuConfigVarsExist := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val appName = (Compile / herokuAppName).value
  val svc = url(s"https://api.heroku.com/apps/$appName/config-vars")
    .addHeader("Accept", "application/vnd.heroku+json; version=3")
    .addHeader("Authorization", s"Bearer ${herokuApiKey.value}")
    .GET
  val result =
    Await.result(Http.default(svc) map (
      _.getResponseBody.contains("PLAY_HTTP_SECRET")
    ), 30.seconds)
  println(s"$appName Heroku app config vars exist: $result")
  result
}

herokuConfigVarsCreate := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val apiKey = herokuApiKey.value
  val appName = (Compile / herokuAppName).value
  val stagePlayHttpSecretValue =
    stagePlayHttpSecret.value
  val stagePlayMailerUserValue =
    stagePlayMailerUser.value
  val stagePlayMailerPasswordValue =
    stagePlayMailerPassword.value
  val stageSilhouetteAuthenticatorSignerKeyValue =
    stageSilhouetteAuthenticatorSignerKey.value
  val stageSilhouetteAuthenticatorCrypterKeyValue =
    stageSilhouetteAuthenticatorCrypterKey.value
  val result =
    if (herokuAppExists.value) {
      val svc = url(s"https://api.heroku.com/apps/$appName/config-vars")
        .addHeader("Accept", "application/vnd.heroku+json; version=3")
        .addHeader("Authorization", s"Bearer $apiKey")
        .addHeader("Content-Type", "application/json")
        .setStringBody(
          s"""{
             |  "HOSTING_DOMAIN":
             |    "$appName.herokuapp.com",
             |  "PLAY_HTTP_SECRET":
             |    "$stagePlayHttpSecretValue",
             |  "PLAY_MAILER_USER":
             |    "$stagePlayMailerUserValue",
             |  "PLAY_MAILER_PASSWORD":
             |    "$stagePlayMailerPasswordValue",
             |  "SILHOUETTE_AUTHENTICATOR_SIGNER_KEY":
             |    "$stageSilhouetteAuthenticatorSignerKeyValue",
             |  "SILHOUETTE_AUTHENTICATOR_CRYPTER_KEY":
             |    "$stageSilhouetteAuthenticatorCrypterKeyValue",
             |  "ENVIRONMENT_ID": "stage",
             |  "HEROKU_APP_NAME": "$appName",
             |  "HEROKU_API_KEY": "$apiKey"
             |}""".stripMargin)
        .PATCH
      Await.result(Http.default(svc) map ( _.getStatusCode == 200), 30.seconds)
    } else false
  println(s"$appName Heroku app config vars created: $result")
  result
}

herokuFormationBatchUpdate := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val apiKey = herokuApiKey.value
  val appName = (Compile / herokuAppName).value
  val result =
    if (herokuAppExists.value) {
      val svc = url(s"https://api.heroku.com/apps/$appName/formation")
        .addHeader("Accept", "application/vnd.heroku+json; version=3")
        .addHeader("Authorization", s"Bearer $apiKey")
        .addHeader("Content-Type", "application/json")
        .setStringBody(
          s"""{
             |  "updates": [
             |    {
             |      "quantity": 1,
             |      "size": "standard-1X",
             |      "type": "web"
             |    },
             |    {
             |      "quantity": 0,
             |      "size": "performance-m",
             |      "type": "worker"
             |    }
             |  ]
             |}""".stripMargin)
        .PATCH
      Await.result(Http.default(svc) map ( _.getStatusCode == 200), 30.seconds)
    } else false
  println(s"$appName Heroku app config vars created: $result")
  result
}
