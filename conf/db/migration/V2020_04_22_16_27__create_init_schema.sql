CREATE TABLE "AuthToken" (
                             "id" UUID  NOT NULL PRIMARY KEY,
                             "userId" integer NOT NULL,
                             "expiry" timestamp without time zone NOT NULL
);

CREATE TABLE "CookieAuthenticator" (
                                       "id" VARCHAR NOT NULL PRIMARY KEY,
                                       "providerID" character varying NOT NULL,
                                       "providerKey" character varying NOT NULL,
                                       "lastUsedDateTime" timestamp without time zone NOT NULL,
                                       "expirationDateTime" timestamp without time zone NOT NULL,
                                       "idleTimeout" bigint,
                                       "cookieMaxAge" bigint,
                                       "fingerprint" character varying
);

CREATE TABLE "User" (
                        "id" SERIAL NOT NULL PRIMARY KEY,
                        "providerID" character varying NOT NULL,
                        "providerKey" character varying NOT NULL,
                        "firstName" character varying,
                        "lastName" character varying,
                        "fullName" character varying,
                        "email" character varying,
                        "activated" boolean NOT NULL,
                        "hasher" character varying NOT NULL,
                        "password" character varying NOT NULL,
                        "salt" character varying,
                        "isAdmin" boolean NOT NULL
);