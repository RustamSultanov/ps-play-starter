Play App Template
=================

This is a sample app with Silhouette, Slick and compile-time dependency
injection using macwire.

Required Tools
--------------

The following tools are required to set up the local development environment:

- [Git](https://git-scm.com/)
- [sbt](https://www.scala-sbt.org/)
- [PostgreSQL](https://www.postgresql.org/)

Developer Environment Set Up
------------------------------

The following steps will allow you to start developing in a local environment

1. Install the required tools (see above).
1. Clone the repository.
    - It is a good idea to mark the cloned source code folder as encrypted.
1. Create an empty database on the PostgreSQL server.
1. Create a local environment configuration file `application.local.conf` taking
   [`application.local.sample.conf`](conf/application.local.sample.conf)
   as example (see also [Environments](environments#local)).
    - change `slick.dbs.default.db.url` to configure connection to the
      previously created database
1. Run the local application instance by typing the following in the sbt
   console:
```
run -Dconfig.resource=application.local.conf
```
The application will be accessible at http://127.0.0.1:9000.
1. Navigate to http://127.0.0.1:9000/signUp and sign up a new admin user. Any
   email and password will be accepted.
    - To complete the sign up process a email confirmation may be needed. Since
      by default emails are suppressed in local environment you will have to
      find the confirmation link in the sbt output window where all the
      suppressed emails are logged.

Now you are able to use your local developer application instance.

See the 
[Contribution Guide](CONTRIBUTING.md) for further instructions.
