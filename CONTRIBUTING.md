Contribution Guide
==================

See [README.md](README.md) for instructions on the 
development environment setup.

All the changes to the source code must be done in separate branches and 
proposed via 
[merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/) into 
the master branch.

Please make sure that your code is clean and works prior to
submitting a merge request:

- check manually that your changes work as expected on your local instance
- run unit tests:
```
sbt testQuick
```
- try to fix all the relevant compilation warnings and IDE suggestions

Code in the master branch should always be in a consistent state and be ready to
be deployed into production. Thus all the proposed merge requests should contain 
the code which is consistent and error free. Nevertheless it is always better to
have smaller commits. Always try to:

- split a large amount of work into smaller chunks
- submit merge requests more often
- keeping each change as small as possible as long as it is consistent and 
  represents a (small) finished piece of work.

Coding Style
------------

- Use two spaces for indentation in all the source files.
- In Scala code use round brackets `()` instead of curly brackets `{}` where
  possible.
- Use strongest possible access modifiers (`private` where possible). This makes
  recompilation faster.
- [Trailing commas](https://github.com/scala/docs.scala-lang/pull/533) are 
  welcome.
- Remove unnecessary symbols.
- The code has to be self-explanatory. Break large procedures/functions into
  smaller parts representing simple computation steps with self-explanatory 
  names.
- Keep the code as minimal as possible. 
- Try to avoid code duplication where appropriate, i.e. follow the
  [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) principle.

See also the [Scala Style Guide](https://docs.scala-lang.org/style/).