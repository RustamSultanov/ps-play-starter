package data.db

import com.mohiva.play.silhouette.api.LoginInfo
import models.{User, UserData}
import slick.dbio.DBIO
import data.db.SlickPgProfile.api._

import scala.concurrent.ExecutionContext.Implicits.global

object UserDBIO {

  // TODO: DRY show
  def lookup(key: User.Id): DBIO[Option[User]] =
    for {
      queried <-
        UserDataQueries
          .byUserId(key)
          .result
      result <-
        if (queried.size > 1)
          DBIO failed new Exception(s"Many users with id $key")
        else
          DBIO successful queried.headOption
    } yield result.map(_.toUser)

  def findOne(loginInfo: LoginInfo): DBIO[Option[User]] =
    for {
      queried <-
        UserDataQueries
          .findOne((loginInfo.providerID, loginInfo.providerKey))
          .result
      result <-
        if (queried.size > 1)
          DBIO failed new Exception(s"Many users with id $loginInfo")
        else
          DBIO successful queried.headOption
    } yield result.map(_.toUser)

  def save(entity: User): DBIO[UserData] = {
    for {
      saved <- UserDataDBIO save entity.data
    } yield saved
  }.transactionally

  def update(entity: User): DBIO[User] = {
    for {
      _ <- UserDataDBIO update entity.data
    } yield entity
  }.transactionally

  // TODO: use compiledDelete?
  def deleteById(id: User.Id) =
    UserDataQueries.byUserId(id).delete

}
