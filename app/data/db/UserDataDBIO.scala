package data.db

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import models.{User, UserData}
import slick.dbio.DBIO
import data.db.SlickPgProfile.api._
import data.db.SlickPgProfile.mapping._

import scala.concurrent.ExecutionContext.Implicits.global

object UserDataDBIO {

  def lookup(key: User.Id): DBIO[Option[UserData]] =
    for {
      queried <- UserDataQueries.byUserId(key).result
      result <-
        if (queried.size > 1)
          DBIO failed new Exception(s"Many users with id $key")
        else
          DBIO successful queried.headOption
    } yield result

  def findOne(loginInfo: LoginInfo): DBIO[Option[UserData]] =
    for {
      queried <-
        UserDataQueries
          .findOne((loginInfo.providerID, loginInfo.providerKey))
          .result
      result <-
        if (queried.size > 1)
          DBIO failed new Exception(s"Many users with id $loginInfo")
        else
          DBIO successful queried.headOption
    } yield result

  def save(entity: UserData): DBIO[UserData] =
    for {
      saved <- (
          UserDataQueries.table returning UserDataQueries.table.map(_.id)
            into ((user, id) => user.copy(id = id))
      ) += entity
    } yield saved

  def update(entity: UserData): DBIO[UserData] =
    for {
      count <- UserDataQueries byUserId entity.id update entity
      result <-
        if (count < 1) DBIO failed new Exception("None updated")
        else DBIO successful entity
    } yield result

  def update(
    loginInfo: LoginInfo,
    passwordInfo: PasswordInfo,
  ): DBIO[Int] =
    UserDataQueries
      .passwordInfoByLoginInfo((loginInfo.providerID, loginInfo.providerKey))
      .update(
        (
          passwordInfo.hasher,
          passwordInfo.password,
          passwordInfo.salt,
        ),
      )

  def all = UserDataQueries.table.result

  def existActiveAdmins =
    UserDataQueries.existActiveAdmins.result

  def existActiveAdminsExcept(id: User.Id) =
    UserDataQueries.existActiveAdminsExcept(id).result

  def byEmail(email: String) = UserDataQueries.byEmail(email).result

}
