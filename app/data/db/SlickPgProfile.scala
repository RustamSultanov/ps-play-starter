package data.db

import cats.syntax.either._
import cats.syntax.option._
import com.github.tminglei.slickpg._
import com.github.tminglei.slickpg.agg.PgAggFuncSupport
import com.github.tminglei.slickpg.window.PgWindowFuncSupport
import enumeratum.SlickEnumSupport
import models.Id
import slick.ast.BaseTypedType
import slick.jdbc.{GetResult, JdbcCapabilities}
import be.venneborg.refined.RefinedMapping
import be.venneborg.refined.RefinedSupport
import eu.timepit.refined.types.numeric.PosInt
import io.circe.{Decoder, Encoder, Json}
import io.circe.syntax._
import slick.jdbc.GetResult.GetInt

import scala.reflect.ClassTag

trait SlickPgProfile
    extends ExPostgresProfile
    with PgCirceJsonSupport
    with PgArraySupport
    with PgDate2Support
    with PgRangeSupport
    with PgHStoreSupport
    with PgSearchSupport
    with PgNetSupport
    with PgLTreeSupport
    with PgWindowFuncSupport
    with PgAggFuncSupport
    with SlickEnumSupport
    with RefinedMapping
    with RefinedSupport {

  def pgjson = "jsonb"

  // Add back `capabilities.insertOrUpdate` to enable native `upsert` support; for postgres 9.5+
  override protected def computeCapabilities =
    super.computeCapabilities + JdbcCapabilities.insertOrUpdate

  override val api = SlickPgApi

  object SlickPgApi
      extends API
      with JsonImplicits
      with CirceJsonPlainImplicits
      with ArrayImplicits
      with DateTimeImplicits
      with NetImplicits
      with LTreeImplicits
      with RangeImplicits
      with HStoreImplicits
      with SearchImplicits
      with SearchAssistants
      with Date2DateTimePlainImplicits
      with GeneralAggFunctions
      with RefinedImplicits {

    implicit val strListTypeMapper: DriverJdbcType[List[String]] =
      new SimpleArrayJdbcType[String]("text").to(_.toList)

    implicit val posInfGetResult: GetResult[PosInt] =
      GetInt.andThen(PosInt.unsafeFrom)

    implicit def idMapping[T, U: BaseColumnType]: BaseTypedType[Id[T, U]] =
      MappedColumnType.base[Id[T, U], U](
        _.value,
        Id.apply,
      )

    implicit def idGetResult[TRef, TVal](implicit
      getTValResult: GetResult[TVal],
    ): GetResult[Id[TRef, TVal]] =
      getTValResult.andThen(Id.apply)

    implicit final class JsonRepOps(rep: Rep[Json]) {
      def jsonCodecMapping[T: ClassTag: Encoder: Decoder] =
        rep.<>[T](
          (_: Json)
            .as[T].valueOr(error =>
              throw new Exception(
                s"Error decoding presisted JSON in DB",
                error,
              ),
            ),
          (_: T).asJson.some,
        )
    }

  }

}

object SlickPgProfile extends SlickPgProfile
