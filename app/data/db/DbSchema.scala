package data.db

object DbSchema {

  lazy val createStatementsList = (
    UserDataQueries.schema.createStatements ++
      AuthTokenQueries.schema.createStatements ++
      CookieAuthenticatorQueries.schema.createStatements
  ).toList

}
