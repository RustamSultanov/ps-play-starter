package actions

import cats.implicits._
import mouse.boolean._
import play.api.Environment
import play.api.Mode.Prod
import play.api.http.{HttpErrorHandler, Status}
import play.api.mvc.{ActionFilter, Request, Result}

import scala.concurrent.{ExecutionContext, Future}
import scala.language.higherKinds

case class DevOnlyAction[R[X] <: Request[X]](
  environment: Environment,
)(implicit
  val executionContext: ExecutionContext,
  httpErrorHandler: HttpErrorHandler,
) extends ActionFilter[R] {
  override protected def filter[A](request: R[A]): Future[Option[Result]] =
    (environment.mode == Prod)
      .option(
        httpErrorHandler
          .onClientError(
            request: Request[A],
            Status.NOT_FOUND,
            "not accessible in production",
          ),
      )
      .sequence
}
