package actions

import cats.implicits._
import data.db.UserDBIO
import models.User
import play.api.http.{HttpErrorHandler, Status}
import play.api.mvc.{ActionRefiner, Request}
import slick.jdbc.JdbcBackend
import utils.ValueRequest

import scala.concurrent.{ExecutionContext, Future}

case class UserAction[R[X] <: Request[X]](
  db: JdbcBackend#DatabaseDef,
  id: User.Id,
)(implicit
  val executionContext: ExecutionContext,
  httpErrorHandler: HttpErrorHandler,
) extends ActionRefiner[
      R,
      ({ type T[B] = ValueRequest[User, R, B] })#T,
    ] {
  def refine[A](request: R[A]) =
    for {
      maybeEntity <- db.run(UserDBIO.lookup(id))
      refined <-
        maybeEntity
          .map(entity => Future.successful(new ValueRequest(entity, request)))
          .toRight(
            httpErrorHandler
              .onClientError(
                request: Request[A],
                Status.NOT_FOUND,
                "Пользователь не найден",
              ),
          )
          .bisequence
    } yield refined
}
