package jobs.silhouette

import akka.actor.ActorSystem
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension

// TODO get rid of quartz scheduling

/**
 * Schedules the jobs.
 */
class Scheduler(
  system: ActorSystem,
  authTokenCleanerWrapper: AuthTokenCleanerWrapper,
) {

  lazy val authTokenCleaner = authTokenCleanerWrapper.underlying

  QuartzSchedulerExtension(system).schedule(
    "AuthTokenCleaner",
    authTokenCleaner,
    AuthTokenCleaner.Clean,
  )

  authTokenCleaner ! AuthTokenCleaner.Clean

}
