package jobs
import java.util.concurrent.RejectedExecutionException

import play.api.Logger

// TODO: find ways to avoid such exceptions alltogether

object IsIgnoredRejectedExecutionException {

  def unapply(exception: RejectedExecutionException) =
    exception match {
      case IsTerminatedPoolRejectionException(matched)   => Some(matched)
      case IsShuttingDownPoolRejectionException(matched) => Some(matched)
      case _                                             => None
    }

}

class IgnoredException(name: String, throwable: Throwable) {
  private lazy val logger = Logger(getClass)
  def ignore() =
    logger.warn(
      s"ignored $name exception",
      throwable,
    )
}

class TerminatedPoolRejectionException(exception: RejectedExecutionException)
    extends IgnoredException("task rejected from terminated pool", exception)

object IsTerminatedPoolRejectionException {

  def unapply(exception: RejectedExecutionException) =
    if (exception.getMessage.contains("[Terminated, pool size = "))
      Some(new TerminatedPoolRejectionException(exception))
    else
      None

}

class ShuttingDownPoolRejectionException(exception: RejectedExecutionException)
    extends IgnoredException("task rejected from shutting down pool", exception)

object IsShuttingDownPoolRejectionException {

  def unapply(exception: RejectedExecutionException) =
    if (exception.getMessage.contains("[Shutting down, pool size = "))
      Some(new TerminatedPoolRejectionException(exception))
    else
      None

}
