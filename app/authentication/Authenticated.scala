package authentication

import com.mohiva.play.silhouette.api.actions.{SecuredRequest, UserAwareRequest}
import models.User
import play.api.mvc.RequestHeader
import silhouetteIntegration.DefaultEnv
import utils.TypedWrappedRequest

import scala.language.reflectiveCalls

final case class Authenticated(user: User)

object Authenticated extends UnAuthenticated {
  implicit def secured(implicit
    request: SecuredRequest[DefaultEnv, _],
  ): Authenticated =
    apply(request.identity)
  implicit def secured_wrapped(implicit
    request: TypedWrappedRequest[
      ({ type S[X] = SecuredRequest[DefaultEnv, X] })#S,
      _,
    ],
  ): Authenticated =
    apply(request.request.identity)
  implicit def aware(implicit
    request: UserAwareRequest[DefaultEnv, _],
  ): Option[Authenticated] =
    request.identity map apply
  implicit def relax(implicit
    authenticated: Authenticated,
  ): Option[Authenticated] =
    Some(authenticated)
}

trait UnAuthenticated {
  implicit def secured(implicit request: RequestHeader) =
    Option.empty[Authenticated]
}
