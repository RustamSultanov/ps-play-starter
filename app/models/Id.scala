package models

import cats.Order
import eu.timepit.refined.types.numeric.PosInt
import io.circe.{Decoder, Encoder}
import mouse.string._
import pureconfig.ConfigReader
import pureconfig.configurable.genericMapReader
import pureconfig.error.CannotConvert
import pureconfig.generic.semiauto.deriveReader

final case class Id[TRef, TId](value: TId) extends AnyVal {
  override def toString: String = value.toString
}

object Id {

  def of[TRef] = new IdConstructor[TRef]

  implicit def idOrder[TRef, TId: Order]: Order[Id[TRef, TId]] =
    Order.by(_.value)

  implicit def idConfigReader[TRef, TId: ConfigReader]
    : ConfigReader[Id[TRef, TId]] =
    deriveReader[Id[TRef, TId]]

  implicit def posIntIdMapReader[TRef, TVal: ConfigReader] =
    genericMapReader[Id[TRef, PosInt], TVal](str =>
      str.parseInt.left
        .map(e => CannotConvert(str, "Int", e.getMessage))
        .flatMap(
          PosInt
            .from(_).left
            .map(s => CannotConvert(str, "PosInt", s)),
        )
        .map(Id.apply),
    )

  implicit def idDecoder[TRef, TValue](implicit valueDecoder: Decoder[TValue]) =
    valueDecoder.map(models.Id.of[TRef].valued)
  implicit def idEncoder[TRef, TValue](implicit valueEncoder: Encoder[TValue]) =
    Encoder.instance[models.Id[TRef, TValue]](id => valueEncoder(id.value))

}

class IdConstructor[TRef] {
  def valued[TId](value: TId) = new Id[TRef, TId](value)
}

final case class Identifiable[TRef, TId](id: Id[TRef, TId], data: TRef)
