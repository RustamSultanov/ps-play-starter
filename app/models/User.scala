package models

import java.util.UUID

import com.mohiva.play.silhouette.api.util.{PasswordHasher, PasswordInfo}
import com.mohiva.play.silhouette.api.{Identity, LoginInfo}
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import org.joda.time.{DateTime, DateTimeZone}

import scala.concurrent.duration.Duration

case class User(
  data: UserData,
) extends Identity

object User {
  type Id = models.Id[User, Int]
}

case class UserData(
  id: User.Id = Id(-1),
  providerID: String,
  // TODO: is providerKey always the same as email?
  providerKey: String,
  firstName: Option[String] = None,
  lastName: Option[String] = None,
  fullName: Option[String] = None,
  email: String,
  activated: Boolean,
  hasher: String,
  password: String,
  salt: Option[String] = None,
  isAdmin: Boolean = false,
) extends Identity {

  val toUser = User(this)

  lazy val loginInfo = LoginInfo(providerID, providerKey)

  lazy val passwordInfo = PasswordInfo(hasher, password, salt)

  /**
   * Tries to construct a name.
   *
   * @return Maybe a name.
   */
  def name =
    fullName.orElse {
      firstName -> lastName match {
        case (Some(f), Some(l)) => Some(f + " " + l)
        case (Some(f), None)    => Some(f)
        case (None, Some(l))    => Some(l)
        case _                  => None
      }
    }

  def freshToken(lifespan: Duration = AuthToken.defaultLifespan) =
    AuthToken(
      UUID.randomUUID(),
      id,
      DateTime.now withZone DateTimeZone.UTC plusSeconds
        lifespan.toSeconds.toInt,
    )

}

object UserData {

  def withPassword(passwordHasher: PasswordHasher)(
    email: String,
    isAdmin: Boolean,
    password: String,
    activated: Boolean,
  ) = {
    val passwordInfo = passwordHasher hash password
    withLoginInfo(
      email = email,
      isAdmin = isAdmin,
      loginInfo = LoginInfo(CredentialsProvider.ID, email),
      hasher = passwordInfo.hasher,
      password = passwordInfo.password,
      salt = passwordInfo.salt,
      activated = activated,
    )
  }

  def withLoginInfo(
    userId: User.Id = Id(-1),
    loginInfo: LoginInfo,
    firstName: Option[String] = None,
    lastName: Option[String] = None,
    fullName: Option[String] = None,
    email: String,
    activated: Boolean,
    hasher: String,
    password: String,
    salt: Option[String] = None,
    isAdmin: Boolean = false,
  ) =
    UserData(
      userId,
      loginInfo.providerID,
      loginInfo.providerKey,
      firstName,
      lastName,
      fullName,
      email,
      activated,
      hasher,
      password,
      salt,
      isAdmin,
    )

}
