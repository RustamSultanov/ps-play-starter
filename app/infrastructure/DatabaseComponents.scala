package infrastructure

import slick.basic.DatabaseConfig
import slick.jdbc.{JdbcBackend, JdbcProfile}

trait DatabaseComponents {
  protected def dbConfig: DatabaseConfig[JdbcProfile]
  implicit lazy val db: JdbcBackend#DatabaseDef =
    dbConfig.db
  def close() = db.close
}
