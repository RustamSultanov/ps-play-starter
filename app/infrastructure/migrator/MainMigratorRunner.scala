package infrastructure.migrator

import play.api.{Configuration, Logger}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

object MainMigratorRunner {

  private lazy val logger = Logger(getClass)

  def main(args: Array[String]): Unit = {
    // for some reason a Future is needed for Heroku to kill the dyno
    // when the program ends
    Future(logger info "started")
    val classLoader = getClass.getClassLoader
    val config = Configuration.load(
      classLoader,
      System.getProperties,
      Map.empty[String, AnyRef],
      false,
    )
    val components = new MigratorLoader(config)
    components.flyway.migrate
    logger info "exiting"
  }

}
