package infrastructure.migrator

import com.typesafe.config.ConfigFactory
import config.FlywayConfig
import infrastructure.TimeZoneComponents
import org.flywaydb.core.Flyway
import play.api.Configuration

//noinspection ScalaUnusedSymbol
final class MigratorLoader(config: Configuration)
    extends TimeZoneComponents
    with MigrationComponents {

  lazy val configuration: Configuration = config

  protected lazy val flywayConfig: FlywayConfig =
    FlywayConfig.from(configuration)

}

object MigratorLoader {
  def from(configResouce: String) =
    new MigratorLoader(Configuration(ConfigFactory load configResouce))
}

trait MigrationComponents {

  protected def flywayConfig: FlywayConfig

  val flyway = Flyway
    .configure()
    .dataSource(flywayConfig.url, flywayConfig.user, flywayConfig.password)
    .baselineOnMigrate(true)
    .outOfOrder(true)
    .ignoreMissingMigrations(true)
    .load()

}
