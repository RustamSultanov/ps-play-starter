package infrastructure

import com.softwaremill.macwire._
import controllers._
import controllers.auth._
import infrastructure.migrator.MigrationComponents
import org.webjars.play.{RequireJS, WebJarComponents}
import play.api.ApplicationLoader.Context
import play.api._
import play.api.libs.mailer.MailerComponents
import play.api.libs.ws.ahc.AhcWSComponents
import play.filters.csrf.CSRFComponents
import play.filters.headers.SecurityHeadersComponents
import play.filters.hosts.AllowedHostsComponents
import play.filters.https.RedirectHttpsComponents
import silhouetteIntegration.errorHandlers.CustomHttpErrorHandler
import utils.{FrontComponents, FrontConfig}

import scala.concurrent.Await
import scala.concurrent.duration._

final class MainApplicationLoader extends ApplicationLoader {
  def load(context: Context) = {
    LoggerConfigurator(context.environment.classLoader).foreach {
      _.configure(context.environment, context.initialConfiguration, Map.empty)
    }
    new MainComponents(context).application
  }
}

//noinspection ScalaUnusedSymbol
final class MainComponents(context: Context)
    extends BuiltInComponentsFromContext(context)
    with CSRFComponents
    with SecurityHeadersComponents
    with AllowedHostsComponents
    with RedirectHttpsComponents
    with MailerComponents
    with SilhouetteComponents
    with AssetsComponents
    with WebJarComponents
    with PlayDatabaseComponents
    with MigrationComponents
    with RollbarComponents
    with AhcWSComponents
    with TimeZoneComponents
    with RootConfigComponents {

  flyway.migrate()

  implicit private lazy val implicitWebJarsUtil = webJarsUtil
  implicit private lazy val implicitAssetsFinder = assetsFinder

  implicit lazy val frontComponents: FrontComponents =
    new FrontComponents()

  lazy val httpFilters = Seq(
    csrfFilter,
    securityHeadersFilter,
    allowedHostsFilter,
    redirectHttpsFilter,
  )

  private lazy val topController: TopController = wire[TopController]
  private lazy val signUpController = wire[SignUpController]
  private lazy val signInController = wire[SignInController]
  private lazy val forgotPasswordController = wire[ForgotPasswordController]
  private lazy val resetPasswordController = wire[ResetPasswordController]
  private lazy val changePasswordController = wire[ChangePasswordController]
  private lazy val activateAccountController = wire[ActivateAccountController]
  private lazy val selfController = wire[SelfController]
  private lazy val userController = wire[UserController]
  private lazy val guestController = wire[GuestController]

  private lazy val rollbarConfigController = wire[RollbarConfigController]
  private lazy val schemaController = wire[SchemaController]

  private lazy val requireJS = wire[RequireJS]
  private lazy val webjarsRouter = wire[webjars.Routes]

  implicit override lazy val httpErrorHandler =
    new CustomHttpErrorHandler(
      environment,
      configuration,
      rollbar,
      devContext.map(_.sourceMapper),
      Some(router),
      messagesApi,
    )

  private lazy val routePrefix = "/"
  lazy val router = wire[_root_.router.Routes]

}
