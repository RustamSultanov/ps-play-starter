package infrastructure

import play.api.db.slick.{DbName, SlickComponents}
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

trait PlayDatabaseComponents extends SlickComponents with DatabaseComponents {
  protected lazy val dbConfig: DatabaseConfig[JdbcProfile] =
    slickApi.dbConfig(DbName("default"))
}
