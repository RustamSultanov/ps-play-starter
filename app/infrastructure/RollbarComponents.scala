package infrastructure

import com.rollbar.Rollbar
import config.{HostingConfig, RollbarConfig}
import play.api.Configuration
import com.softwaremill.macwire._

trait RollbarComponents {

  def configuration: Configuration

  def hostingConfig: HostingConfig

  lazy val rollbarConfig: Option[RollbarConfig] =
    wireWith(RollbarConfig.fromConfig _)
  implicit lazy val rollbar: Option[Rollbar] = rollbarConfig.map(_.rollbar)
}
