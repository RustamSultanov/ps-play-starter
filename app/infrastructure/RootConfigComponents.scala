package infrastructure

import config._
import play.api.Configuration
import pureconfig.ConfigSource
import utils.FrontConfig

trait RootConfigComponents {

  def configuration: Configuration

  lazy val config = configuration.underlying
  implicit lazy val flywayConfig = FlywayConfig from configuration
  implicit lazy val frontConfig = FrontConfig from configuration

  lazy val configSource = ConfigSource.fromConfig(configuration.underlying)
  lazy val rootConfig = configSource.loadOrThrow[RootConfig]

  implicit lazy val hostingConfig =
    rootConfig.hosting
}
