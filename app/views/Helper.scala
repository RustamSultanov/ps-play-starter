package views

import java.time.format.DateTimeFormatter
import java.time.{Duration, LocalDateTime}

import cats.data.NonEmptyList
import play.api.i18n.Messages
import play.twirl.api.Html

// TODO: DRY with tylip

object Helper {

  implicit class NeList[T](val nel: NonEmptyList[T]) extends AnyVal {
    def separated(separator: Html = Html(", "))(
      render: T => Html,
    ): Html =
      nel.tail match {
        case Nil =>
          render(nel.head)
        case tailHead :: tailTail =>
          Html(
            render(nel.head).body + separator.body +
              neListSep(tailHead, tailTail)(
                render,
              )(separator).body,
          )
      }
  }

  def titleMessage(id: String)(implicit messages: Messages) =
    if (Messages.isDefinedAt(s"$id.title")) messages(s"$id.title")
    else id.split('.').lastOption.getOrElse(id)

  def neListSep[T](head: T, tail: Seq[T])(
    render: T => Html,
  )(separator: Html = Html(", ")): Html =
    tail match {
      case Nil =>
        render(head)
      case tailHead :: tailTail =>
        Html(
          render(head).body + separator.body + neListSep(tailHead, tailTail)(
            render,
          )(separator).body,
        )
    }

  def neList[T](head: T, tail: Seq[T])(
    render: T => Html,
  ): Html =
    tail match {
      case Nil =>
        render(head)
      case tailHead :: tailTail =>
        val separator: Html = Html(", ")
        Html(
          render(head).body + separator.body + neListSep(tailHead, tailTail)(
            render,
          )(separator).body,
        )
    }

  def list[T](
    elements: List[T],
  )(
    render: T => Html,
  )(
    empty: Html = Html(""),
  ): Html =
    elements match {
      case Nil          => empty
      case head :: tail => neList(head, tail)(render)
    }

  val dateTimePattern = "dd.MM.yyyy HH:mm"
  val dateTimeFormat = DateTimeFormatter.ofPattern(dateTimePattern)

  def dateTime(dateTime: LocalDateTime) =
    dateTime.format(dateTimeFormat)

  // https://stackoverflow.com/a/40487511/707926
  def duration(duration: Duration): String =
    duration.toString
      .substring(2)
      .replaceAll("(\\d[HMS])(?!$)", "$1 ")
      .toLowerCase

  def momentsLocale(messages: Messages) =
    messages.lang.language match {
      case "en"  => "en-gb"
      case other => other
    }

}
