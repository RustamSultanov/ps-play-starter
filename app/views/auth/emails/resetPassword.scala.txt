@import play.api.i18n.Messages

@(user: models.UserData, url: String)(implicit messages: Messages)
@messages(
  "email.activate.account.hello",
  user.name.map(name => s" $name")
  .getOrElse("")
)

@messages("email.reset.password.txt.text", url)
