package config

import play.api.Configuration

final case class FlywayConfig(url: String, user: String, password: String)

object FlywayConfig {
  def from(
    configuration: Configuration,
  ) =
    new FlywayConfig(
      configuration.get[String]("slick.dbs.default.db.url"),
      configuration.get[String]("slick.dbs.default.db.user"),
      configuration.get[String]("slick.dbs.default.db.password"),
    )
}
