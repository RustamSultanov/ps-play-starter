import enumeratum.{Enum, EnumEntry}
import eu.timepit.refined.types.numeric.PosInt
import models.Id
import play.api.ConfigLoader

package object config {

  implicit val posIntLoader: ConfigLoader[PosInt] =
    ConfigLoader(config => path => PosInt unsafeFrom config.getInt(path))

  implicit def idLoader[TRef, TVal](implicit
    valLoader: ConfigLoader[TVal],
  ): ConfigLoader[Id[TRef, TVal]] =
    ConfigLoader(config => path => Id(valLoader.load(config, path)))

  def enumLoader[T <: EnumEntry](enum: Enum[T]): ConfigLoader[T] =
    implicitly[ConfigLoader[String]].map(enum.withName)

}
