package config

import com.rollbar.Rollbar
import com.rollbar.payload.data.Server
import play.api.Configuration

case class RollbarConfig(
  accessToken: String,
  clientAccessToken: String,
  environment: String,
  serverHost: String,
) {
  lazy val rollbar =
    new Rollbar(accessToken, environment)
      .server(new Server().host(serverHost))
}

object RollbarConfig {
  def fromConfig(
    configuration: Configuration,
    hostingConfig: HostingConfig,
  ) =
    for {
      accessToken <- configuration.getOptional[String]("rollbar.accessToken")
      clientAccessToken <- configuration.getOptional[String](
        "rollbar.clientAccessToken",
      )
    } yield
      new RollbarConfig(
        accessToken,
        clientAccessToken,
        configuration
          .getOptional[String]("rollbar.environment").getOrElse("unknown"),
        hostingConfig.host,
      )
}
