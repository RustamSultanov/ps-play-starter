package config

import pureconfig.generic.ProductHint
import pureconfig.{CamelCase, ConfigFieldMapping}

trait CamelCaseProductHint {
  implicit def productHint[T] =
    ProductHint[T](
      fieldMapping = ConfigFieldMapping(CamelCase, CamelCase),
    )
}
