package config

import pureconfig.generic.semiauto.deriveReader

final case class RootConfig(
  hosting: HostingConfig,
)

object RootConfig extends CamelCaseProductHint {
  implicit val rootConfigReader = deriveReader[RootConfig]
}
