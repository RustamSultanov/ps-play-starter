package forms

import play.api.data.Form
import play.api.data.Forms._

object InviteForm {
  lazy val form = Form("email" -> email)
}
