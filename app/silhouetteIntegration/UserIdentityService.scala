package silhouetteIntegration

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.services.IdentityService
import data.db.UserDBIO
import models.User
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcBackend

import scala.concurrent.{ExecutionContext, Future}

class UserIdentityService(
  db: JdbcBackend#DatabaseDef,
)(implicit ex: ExecutionContext)
    extends IdentityService[User] {

  def retrieve(loginInfo: LoginInfo): Future[Option[User]] =
    db.run(UserDBIO.findOne(loginInfo))

}
