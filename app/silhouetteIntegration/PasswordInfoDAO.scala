package silhouetteIntegration

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.persistence.daos.DelegableAuthInfoDAO
import data.db.UserDataDBIO
import slick.jdbc.JdbcBackend

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.reflect.ClassTag

class PasswordInfoDAO(db: JdbcBackend#DatabaseDef)
    extends DelegableAuthInfoDAO[PasswordInfo] {

  val classTag: ClassTag[PasswordInfo] = implicitly

  override def find(loginInfo: LoginInfo): Future[Option[PasswordInfo]] =
    db.run(UserDataDBIO.findOne(loginInfo)) map
      (_ map (_.passwordInfo))

  override def add(
    loginInfo: LoginInfo,
    authInfo: PasswordInfo,
  ): Future[PasswordInfo] =
    update(loginInfo, authInfo)

  override def update(
    loginInfo: LoginInfo,
    authInfo: PasswordInfo,
  ): Future[PasswordInfo] =
    db.run(UserDataDBIO.update(loginInfo, authInfo)).map { _ =>
      authInfo
    }

  override def save(
    loginInfo: LoginInfo,
    authInfo: PasswordInfo,
  ): Future[PasswordInfo] =
    update(loginInfo, authInfo)

  override def remove(loginInfo: LoginInfo): Future[Unit] = ???

}
