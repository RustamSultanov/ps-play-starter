package controllers

import com.mohiva.play.silhouette.api.{LogoutEvent, Silhouette}
import com.mohiva.play.silhouette.api.actions.SecuredRequest
import org.webjars.play.WebJarsUtil
import utils.FrontComponents
import play.api.i18n.I18nSupport
import play.api.mvc.{AnyContent, BaseController, ControllerComponents}
import silhouetteIntegration.DefaultEnv

final class SelfController(
  val controllerComponents: ControllerComponents,
  silhouette: Silhouette[DefaultEnv],
)(implicit
  frontComponents: FrontComponents,
) extends BaseController
    with I18nSupport {

  def show =
    silhouette.SecuredAction(implicit request => Ok(views.html.self.show()))

  def signOut =
    silhouette.SecuredAction.async {
      implicit request: SecuredRequest[DefaultEnv, AnyContent] =>
        val result = Redirect(routes.TopController.index())
        silhouette.env.eventBus.publish(LogoutEvent(request.identity, request))
        silhouette.env.authenticatorService
          .discard(request.authenticator, result)
    }

}
