package controllers

import actions.DevOnlyAction
import com.mohiva.play.silhouette.api.Silhouette
import play.api.http.HttpErrorHandler
import play.api.i18n.I18nSupport
import play.api.libs.mailer.MailerClient
import play.api.mvc.{BaseController, ControllerComponents, Request}
import play.api.{Environment, Logger}
import silhouetteIntegration.DefaultEnv
import utils.FrontComponents
import utils.MainViewContext._

import authentication.Authenticated._

import scala.concurrent.Future

final class TopController(
  val controllerComponents: ControllerComponents,
  silhouette: Silhouette[DefaultEnv],
  environment: Environment,
  mailerClient: MailerClient,
)(implicit
  frontComponents: FrontComponents,
  httpErrorHandler: HttpErrorHandler,
) extends BaseController
    with I18nSupport {

  implicit val executionContext = controllerComponents.executionContext

  private lazy val logger = Logger(this.getClass)

  def index =
    silhouette.UserAwareAction(implicit request => Ok(views.html.top.index()))

  def deprecated(redirect: String) =
    Action { implicit request =>
      logger warn s"deprecated URI hit: ${request.uri}"
      PermanentRedirect(redirect)
    }

  def never =
    Action.andThen(DevOnlyAction[Request](environment)).async {
      implicit request =>
        Future.never
    }

  def badRequest =
    Action.andThen(DevOnlyAction[Request](environment)).async {
      implicit request =>
        httpErrorHandler
          .onClientError(request, BAD_REQUEST, "test of bad request")
    }

  def forbidden =
    Action.andThen(DevOnlyAction[Request](environment)).async {
      implicit request =>
        httpErrorHandler.onClientError(request, FORBIDDEN, "test of forbidden")
    }

  def notFound =
    Action.andThen(DevOnlyAction[Request](environment)).async {
      implicit request =>
        httpErrorHandler.onClientError(request, NOT_FOUND, "test of not found")
    }

  def error =
    Action.andThen(DevOnlyAction[Request](environment)).async {
      implicit request =>
        Future.failed(new Exception("test exception"))
    }

}
