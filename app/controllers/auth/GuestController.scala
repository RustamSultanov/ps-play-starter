package controllers.auth

import java.util.UUID

import _root_.actions.WithAdmin
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.util.{
  PasswordHasherRegistry,
  PasswordInfo,
}
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import data.db.{AuthTokenDBIO, UserDataDBIO}
import forms.{GuestForm, InviteForm}
import models.UserData
import play.api.i18n.{I18nSupport, Messages}
import play.api.libs.mailer.{Email, MailerClient}
import play.api.mvc.{AnyContent, BaseController, ControllerComponents, Request}
import silhouetteIntegration.{DefaultEnv, UserIdentityService}
import slick.jdbc.JdbcBackend
import utils.{FrontComponents, StatusType}

import scala.concurrent.Future
import scala.util.Random
import scala.concurrent.duration._

// TODO DRY reset password and guest controller

final class GuestController(
  val controllerComponents: ControllerComponents,
  db: JdbcBackend#DatabaseDef,
  silhouette: Silhouette[DefaultEnv],
  userService: UserIdentityService,
  authInfoRepository: AuthInfoRepository,
  passwordHasherRegistry: PasswordHasherRegistry,
  mailerClient: MailerClient,
)(implicit
  frontComponents: FrontComponents,
) extends BaseController
    with I18nSupport {

  implicit lazy val executionContext = controllerComponents.executionContext

  def create =
    silhouette
      .SecuredAction(WithAdmin)
      .async(implicit request =>
        InviteForm.form
          .bindFromRequest()
          .fold(
            _ =>
              Future successful
                Redirect(controllers.routes.UserController.index())
                  .flashing(StatusType.Danger.entryName -> "Wrong Email"),
            email =>
              for {
                existing <- db run UserDataDBIO.byEmail(email)
                r = new Random
                result <- existing.headOption.fold(
                  for {
                    saved <- db run UserDataDBIO.save(
                      UserData.withPassword(passwordHasherRegistry.current)(
                        email,
                        false,
                        1.to(24).map(_ => r.nextPrintableChar()).mkString,
                        false,
                      ),
                    )
                    authToken <-
                      db.run(AuthTokenDBIO save saved.freshToken(7.days))
                  } yield {
                    val url = routes.GuestController
                      .view(authToken.id)
                      .absoluteURL()
                    mailerClient.send(
                      Email(
                        subject = "Invitation from ps-play-starter.com",
                        from = Messages("email.from"),
                        to = Seq(email),
                        bodyText =
                          Some(views.txt.auth.emails.signUp(saved, url).body),
                        bodyHtml = Some(
                          views.html.auth.emails.signUp(saved, url).body,
                        ),
                      ),
                    )
                    Redirect(controllers.routes.UserController.index())
                      .flashing(
                        StatusType.Success.entryName -> "Invitation sent",
                      )
                  },
                )(_ =>
                  Future successful
                    Redirect(controllers.routes.UserController.index())
                      .flashing(
                        StatusType.Danger.entryName -> Messages(
                          "users.email.alreadyInUse",
                        ),
                      ),
                )
              } yield result,
          ),
      )

  def view(token: UUID) =
    silhouette.UnsecuredAction.async { implicit request: Request[AnyContent] =>
      db.run(AuthTokenDBIO.lookup(token)).map {
        case Some(_) =>
          Ok(views.html.auth.guest(GuestForm.form, token))
        case None =>
          Redirect(routes.SignInController.view())
            .flashing("danger" -> Messages("invalid.reset.link"))
      }
    }

  def promote(token: UUID) =
    silhouette.UnsecuredAction.async { implicit request: Request[AnyContent] =>
      db.run(AuthTokenDBIO.lookup(token)).flatMap {
        case Some(authToken) =>
          GuestForm.form
            .bindFromRequest().fold(
              form =>
                Future successful
                  BadRequest(views.html.auth.guest(form, token)),
              data =>
                db.run(UserDataDBIO.lookup(authToken.userId)) flatMap {
                  case Some(userData)
                      if userData.loginInfo.providerID == CredentialsProvider.ID =>
                    val passwordInfo =
                      passwordHasherRegistry.current.hash(data.password)
                    for {
                      _ <- db.run(
                        UserDataDBIO.update(userData.copy(activated = true)),
                      )
                      _ <-
                        authInfoRepository
                          .update[PasswordInfo](
                            userData.loginInfo,
                            passwordInfo,
                          )
                    } yield
                      Redirect(routes.SignInController.view())
                        .flashing("success" -> Messages("password.reset"))
                  case _ =>
                    Future successful
                      Redirect(routes.SignInController.view())
                        .flashing("danger" -> Messages("invalid.reset.link"))
                },
            )
        case None =>
          Future successful
            Redirect(routes.SignInController.view())
              .flashing("danger" -> Messages("invalid.reset.link"))
      }
    }
}
