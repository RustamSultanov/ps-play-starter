package controllers.auth

import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.actions.SecuredRequest
import com.mohiva.play.silhouette.api.exceptions.ProviderException
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.util.{
  Credentials,
  PasswordHasherRegistry,
  PasswordInfo,
}
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import forms.ChangePasswordForm
import play.api.i18n.{I18nSupport, Messages}
import play.api.mvc.{AnyContent, BaseController, ControllerComponents}
import silhouetteIntegration.{DefaultEnv, WithProvider}
import utils.FrontComponents

import scala.concurrent.Future

final class ChangePasswordController(
  val controllerComponents: ControllerComponents,
  silhouette: Silhouette[DefaultEnv],
  credentialsProvider: CredentialsProvider,
  authInfoRepository: AuthInfoRepository,
  passwordHasherRegistry: PasswordHasherRegistry,
)(implicit
  frontComponents: FrontComponents,
) extends BaseController
    with I18nSupport {

  implicit lazy val executionContext = controllerComponents.executionContext

  /**
   * Views the `Change Password` page.
   *
   * @return The result to display.
   */
  def view =
    silhouette
      .SecuredAction(WithProvider[DefaultEnv#A](CredentialsProvider.ID)) {
        implicit request: SecuredRequest[DefaultEnv, AnyContent] =>
          Ok(
            views.html.auth
              .changePassword(ChangePasswordForm.form, request.identity),
          )
      }

  /**
   * Changes the password.
   *
   * @return The result to display.
   */
  def submit =
    silhouette
      .SecuredAction(WithProvider[DefaultEnv#A](CredentialsProvider.ID))
      .async { implicit request: SecuredRequest[DefaultEnv, AnyContent] =>
        ChangePasswordForm.form
          .bindFromRequest().fold(
            form =>
              Future.successful(
                BadRequest(
                  views.html.auth.changePassword(form, request.identity),
                ),
              ),
            password => {
              val (currentPassword, newPassword) = password
              val credentials =
                Credentials(request.identity.data.email, currentPassword)
              credentialsProvider
                .authenticate(credentials)
                .flatMap { loginInfo =>
                  val passwordInfo =
                    passwordHasherRegistry.current.hash(newPassword)
                  authInfoRepository
                    .update[PasswordInfo](loginInfo, passwordInfo)
                    .map { _ =>
                      Redirect(routes.ChangePasswordController.view())
                        .flashing("success" -> Messages("password.changed"))
                    }
                }
                .recover {
                  case _: ProviderException =>
                    Redirect(routes.ChangePasswordController.view()).flashing(
                      "danger" -> Messages("current.password.invalid"),
                    )
                }
            },
          )
      }
}
