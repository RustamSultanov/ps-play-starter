package controllers

import actions.DevOnlyAction
import data.db._
import play.api.Environment
import play.api.http.HttpErrorHandler
import play.api.mvc._

final class SchemaController(
  val controllerComponents: ControllerComponents,
  environment: Environment,
)(implicit
  httpErrorHandler: HttpErrorHandler,
) extends BaseController {

  implicit val executionContext = controllerComponents.executionContext

  def show =
    Action.andThen(DevOnlyAction[Request](environment))(implicit request =>
      Ok(DbSchema.createStatementsList.mkString(";\n\n")),
    )

}
