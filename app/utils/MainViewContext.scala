package utils

import authentication.Authenticated
import controllers.AssetsFinder
import org.webjars.play.WebJarsUtil
import play.api.Configuration
import play.api.i18n.Messages
import play.api.mvc.{Flash, RequestHeader}

class FrontComponents(implicit
  val assets: AssetsFinder,
  val webJarsUtil: WebJarsUtil,
  val frontConfig: FrontConfig,
)

class MainViewContext(implicit
  val request: RequestHeader,
  val flash: Flash,
  val maybeAuthenticated: Option[Authenticated],
  val messages: Messages,
  val frontComponents: FrontComponents,
)

object MainViewContext {

  implicit def pack(implicit
    request: RequestHeader,
    flash: Flash,
    maybeAuthenticated: Option[Authenticated],
    messages: Messages,
    frontComponents: FrontComponents,
  ) = new MainViewContext()

}

class MainViewAuthenticatedContext(implicit
  request: RequestHeader,
  flash: Flash,
  val authenticated: Authenticated,
  messages: Messages,
  frontComponents: FrontComponents,
) extends MainViewContext()(
      request,
      flash,
      Some(authenticated),
      messages,
      frontComponents,
    )

object MainViewAuthenticatedContext {

  implicit def packAuthenticated(implicit
    request: RequestHeader,
    flash: Flash,
    authenticated: Authenticated,
    messages: Messages,
    frontComponents: FrontComponents,
  ) = new MainViewAuthenticatedContext()

}

case class FrontConfig(googleAnalyticsEnabled: Boolean)

object FrontConfig {

  def from(configuration: Configuration) =
    FrontConfig(
      configuration
        .getOptional[Boolean]("front.googleAnalyticsEnabled")
        .getOrElse(false),
    )

}
