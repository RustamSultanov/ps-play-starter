package utils

import enumeratum.EnumEntry.LowerCamelcase
import enumeratum.{Enum, EnumEntry}

sealed trait StatusType extends EnumEntry with LowerCamelcase
object StatusType extends Enum[StatusType] {
  val values = findValues
  case object Default extends StatusType
  case object Info extends StatusType
  case object Success extends StatusType
  case object Danger extends StatusType
  case object Warning extends StatusType
}
