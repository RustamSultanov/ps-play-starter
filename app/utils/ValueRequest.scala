package utils

import play.api.mvc.Request

import scala.language.existentials

class ValueRequest[TValue, R[X] <: Request[X], A](
  val value: TValue,
  request: R[A],
) extends TypedWrappedRequest[R, A](request)

object ValueRequest {
  type Aux[TValue] = ValueRequest[TValue, R, A] forSome { type R[X]; type A }
}
