package utils.route

import java.util.UUID

import models.Id
import play.api.mvc.PathBindable

/**
 * Some route binders.
 */
object Binders {

  /**
   * A `java.util.UUID` bindable.
   */
  implicit object UUIDPathBindable extends PathBindable[UUID] {
    def bind(key: String, value: String) =
      try Right(UUID.fromString(value))
      catch {
        case _: Exception =>
          Left(
            "Cannot parse parameter '" + key + "' with value '" + value + "' as UUID",
          )
      }

    def unbind(key: String, value: UUID): String = value.toString
  }
  implicit def idPathBinder[TRef, TVal](implicit
    valBinder: PathBindable[TVal],
  ): PathBindable[Id[TRef, TVal]] =
    new PathBindable[Id[TRef, TVal]] {
      override def bind(key: String, value: String) =
        valBinder.bind(key, value) map Id.apply
      override def unbind(key: String, id: Id[TRef, TVal]) =
        valBinder.unbind(key, id.value)
    }
}
